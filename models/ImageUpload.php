<?php
																																						
namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;	

class ImageUpload extends Model
{
	public $image;

	public function uploadFile(UploadedFile $file,$curent_image)
		{
			$this->image=$file;

			if($this->validate())
			{
			//var_dump($curent_image);die;
				$this->deleteCurentImage($curent_image);
				
				return $this->SaveImage();			
			}

		}

	public function getFolder()
	{
		return Yii::getAlias('@web') .'uploads/';
	}

	public function generateFilename()
	{
		return strtolower(md5(uniqId($this->image->baseName))).'.'.$this->image->extension;
	}

	public function deleteCurentImage($curent_image)
	{

		if($this->fileExist($curent_image) && $curent_image!="")
			{
				unlink($this->getFolder(). $curent_image);
			}
	}

	public function fileExist($curent_image)
	{
		return file_exists($this->getFolder(). $curent_image);
	}

	public function SaveImage()
	{
		$filename=$this->generateFilename();		
		$this->image->saveAs($this->getFolder(). $filename);

		return $filename;
	}


		public function rules()
    {
        return [
            [['image'], 'required'],
            [['image'], 'file', 'extensions' => 'jpg,png'],
            
        ];
    }
}	

?>