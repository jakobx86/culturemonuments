<?php

namespace app\models;

use Yii;
use yii\data\Pagination;


/**
 * This is the model class for table "monuments".
 *
 * @property integer $id
 * @property integer $protect_num
 * @property integer $type_id
 * @property string $name
 * @property string $picture
 * @property string $location
 * @property integer $district_id
 * @property integer $location_map
 * @property string $date_event
 * @property string $date_open
 * @property string $authors
 * @property string $material
 * @property string $size
 * @property integer $meaning_id
 * @property integer $admin_id
 *
 * @property Admins $admin
 * @property Districts $district
 * @property Meanings $meaning
 * @property Types $type
 */
class Monuments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'monuments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            /*[['protect_num', 'type_id', 'name', 'district_id', 'meaning_id', 'admin_id'], 'required'],
            [['protect_num', 'type_id', 'district_id', 'location_map', 'meaning_id', 'admin_id'], 'integer'],
            [['name', 'location', 'authors'], 'string', 'max' => 255],
            [['picture', 'date_event', 'date_open', 'material', 'size'], 'string', 'max' => 100],
            [['admin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Admins::className(), 'targetAttribute' => ['admin_id' => 'id']],
            [['district_id'], 'exist', 'skipOnError' => true, 'targetClass' => Districts::className(), 'targetAttribute' => ['district_id' => 'id']],
            [['meaning_id'], 'exist', 'skipOnError' => true, 'targetClass' => Meanings::className(), 'targetAttribute' => ['meaning_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Types::className(), 'targetAttribute' => ['type_id' => 'id']],*/
            [['location','name'], 'required'],
            [['date'],'date','format'=>'php:Y-m-d'],
            [['date'],'default','value'=>date('Y-m-d')],
            [['location','name','authors','date_event'], 'string'],
            [['date_open'],'date','format'=>'php:Y-m-d'],
            [['name'], 'string', 'max'=>255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'protect_num' => 'Protect Num',
            'type_id' => 'Type ID',
            'name' => 'Name',
            'picture' => 'Picture',
            'location' => 'Location',
            'district_id' => 'District ID',
            'location_map' => 'Location Map',
            'date_event' => 'Date Event',
            'date_open' => 'Date Open',
            'authors' => 'Authors',
            'material' => 'Material',
            'size' => 'Size',
            'meaning_id' => 'Meaning ID',
            'admin_id' => 'Admin ID',
        ];
    }


    public function getType()
    {
        return $this->hasOne(Types::className(), ['id' => 'type_id']);
    }
    public function getDistrict()
    {
        return $this->hasOne(Districts::className(), ['id' => 'district_id']);
    }
     public function getMeaning()
    {
        return $this->hasOne(Meanings::className(), ['id' => 'meaning_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
   public function saveImage($filename)
   {
     $this->picture=$filename;
     return $this->save(false);
   }

   public function deleteImage()
   {
     $imageUploadModel = new ImageUpload();
     $imageUploadModel->deleteCurentImage($this->picture);
   }

   public function beforeDelete()
   {

        $this->deleteImage();

        return parent::beforeDelete();
   }

   public function getPicture()
   {

       return ($this->picture) ? '/uploads/'.$this->picture : '/no-image.png';
   }

   public function saveType($type_id)
   {
     $type= Types::findOne($type_id);
     if($type!=null)
     {
         $this->link('type',$type);
         return true;
    }
    else return false;
   }
   public function saveDistrict($district_id)
   {
     $district= Districts::findOne($district_id);
     if($district!=null)
     {
         $this->link('district',$district);
         return true;
    }
    else return false;
   }
   public static function getAll ($query,$pagesize=5)
   {
    // get the total number of articles (but do not fetch the article data yet)
            $count = $query->count();

            // create a pagination object with the total count
            $pagination = new Pagination(['totalCount' => $count,'PageSize'=>$pagesize]);

            // limit the query using the pagination and retrieve the articles
            $monuments = $query->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();

     $data['pagination']=$pagination;
     $data['monuments']=$monuments;
     return $data;
   }
   public static function getPopular($limit)
    {
        return Monuments::find()->orderBy('viewed desc')->limit($limit)->all();
    }
    public static function getRecent($limit)
    {
        return Monuments::find()->orderBy('date asc')->limit($limit)->all();
    }

   public static function getFilter($type,$district)
    {
        if(isset($type)&&isset($district))
        $query = Monuments::find()->where(['type_id'=>$type,'district_id'=>$district]);

        else if(isset($type)) 
        {
            $query = Monuments::find()->where(['type_id'=>$type]);
        }
        else if(isset($district))
        {
            $query = Monuments::find()->where(['district_id'=>$district]);
        }
        return $query;
    }

}
