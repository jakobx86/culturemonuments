<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Monuments;

/**
 * MonumentsSearch represents the model behind the search form about `app\models\Monuments`.
 */
class MonumentsSearch extends Monuments
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'protect_num', 'type_id', 'district_id', 'location_map', 'meaning_id', 'admin_id'], 'integer'],
            [['name', 'picture', 'location', 'date_event', 'date_open', 'authors', 'material', 'size'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Monuments::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            
        ]);
        //  var_dump($params);die;
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'protect_num' => $this->protect_num,
            'type_id' => $this->type_id,
            'district_id' => $this->district_id,
            'location_map' => $this->location_map,
            'meaning_id' => $this->meaning_id,
            'admin_id' => $this->admin_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'picture', $this->picture])
            ->andFilterWhere(['like', 'location', $this->location])
            ->andFilterWhere(['like', 'date_event', $this->date_event])
            ->andFilterWhere(['like', 'date_open', $this->date_open])
            ->andFilterWhere(['like', 'authors', $this->authors])
            ->andFilterWhere(['like', 'material', $this->material])
            ->andFilterWhere(['like', 'size', $this->size]);

        return $dataProvider;
    }
}
