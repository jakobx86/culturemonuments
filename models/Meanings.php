<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "meanings".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Monuments[] $monuments
 */
class Meanings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meanings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMonuments()
    {
        return $this->hasMany(Monuments::className(), ['meaning_id' => 'id']);
    }
}
