<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
/**
 * This is the model class for table "admins".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $photo
 *
 * @property Monuments[] $monuments
 */
class Admins extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public $username;
    public $isAdmin=true;
    public static function tableName()
    {
        return 'admins';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'password'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['email', 'password', 'photo'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'password' => 'Password',
            'photo' => 'Photo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMonuments()
    {
        return $this->hasMany(Monuments::className(), ['admin_id' => 'id']);
    }


    public static function findIdentity($id)
    {
        return Admins::findOne($id);

    }
    public static function findIdentityByAccessToken($token,$type=null)
    {

    }

    public function getId()
    {
        return $this->id;
    }
    public function getAuthKey()
    {
        
    }
     public function validateAuthKey($authKey)
    {
        
    }

    public static  function findByUsername($username)
    {
        return Admins::find()->where(['name'=>$username])->one();
    }
    public function validatePassword($password)
    {
        return ($this->password == $password) ? true : false;
    }
}
