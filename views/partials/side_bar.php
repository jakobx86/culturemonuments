<?
use yii\helpers\Url;
?>
<div id="sidebar">
                    <div class="box1">
                    	 <h3>Популярні памятки</h3>
                    	<?php foreach ($popular as $monument) {?>
                    		 <div class="pop_monument">
                            <a href="<?=Url::toRoute(['site/detail','id'=>$monument->id])?>"><img width="175" src="<?= $monument->getPicture();?>"></a>
                            <p class="title"><?php echo $monument->name;?></p>                      
                            <a href="<?=Url::toRoute(['site/detail','id'=>$monument->id])?>"><p>Деталний перегляд</p></a>
                        </div>
                        
                    	<?php }?>
                       
                       
                    </div>
                    <hr style="background:#000; height:2px" />
                    <div class="box recent">
                    <h3>Нещодавні памятки</h3>
                    <?php foreach ($recent as $monument) {?>
                    		 <div class="recent_monument">
                            <a href="<?=Url::toRoute(['site/detail','id'=>$monument->id])?>"><img class="pict" width="85" src="<?= $monument->getPicture();?>"></a>
                            <p class="title"><?php echo $monument->name;?></p>                      
                            <a href="<?=Url::toRoute(['site/detail','id'=>$monument->id])?>"><p>Деталний перегляд</p></a>
                        </div>
                    	<?php }?>                       


                    </div>
     <hr style="background:#000; height:2px" />
    
     
                </div>