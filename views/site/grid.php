<?
 
use app\models\News;
use yii\widgets\ListView;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

use yii\widgets\LinkPager;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
 
?>
<?php  echo $this->render('_search', ['model' => $searchModel,'types'=>$types,'districts'=>$districts]); ?>
<?echo  GridView::widget([		
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => '',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'protect_num',
            'type_id',
            'name',
            
            [
                'format'=>'html',
                'label'=> 'Image',
                'value'=> function($data)
                {
                    return HTML::img($data->getPicture(),['width'=>200]);  
                }

            ],
            
             /*'location',
             'district_id',
             'location_map',
             'date_event',
             'date_open',
             'authors',
             'material',
             'size',
             'meaning_id',
             'admin_id',*/

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>