<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="wrap_buttons">
					<h3 class="buttons"><?= Html::a('Переглянути памятки культури', ['site/list']) ?></h3>
					<h3 class="buttons "><?
					$class='';
					$title=null;
				    if(Yii::$app->user->isGuest){
				    	$class='not-active';
				    	$title='Необхідна авторизація';
				    }
					echo Html::a('Додати памятку культури',array('admin/monuments/',                  ), 
                    array('class'=>$class,
                          'id'=>'id_name',
                          'title' => $title                              
                          ));?></h3>
					
				</div>
				<br />
				<h3 style="margin-top: 20px;"> Офіційні данні кількості пам'яток культури по типу </h3>
				<table>
					<thead>
						<tr>
							<th>Тип памяток</th>
							<th>Кількість зареєстрованих</th>							
						</tr>						
					</thead>
					<tbody>
						<tr>
							<td class="monument_name">Археологіїя</td>
							<td>4425</td>							
						</tr>
						<tr>
							<td class="monument_name">Історія</a></td>
							<td>1201</td>							
						</tr>
						<tr>
							<td class="monument_name">Монументальне мистецтво</a></td>
							<td>92</td>
							
						</tr>	
						<tr>
							<td class="monument_name">Архітекура та містобудування</a></td>
							<td>146</td>
							
						</tr>
						<tr>
							<td class="monument_name">Садово паркове мистуцтво</td>
							<td>19</td>
							
						</tr>
						<tr>
							<td class="monument_name">Ландшафт</td>
							<td>1</td>
							
						</tr>
						<tr>
							<tr>
							<td class="monument_name">Наука і техніка</td>
							<td>1</td>
							
						</tr>
		</tbody>
				</table>
				<h3 style="margin-top: 20px;">Данні кількості пам'яток культури по типу на сайті</h3>
				<table>
					<thead>
						<tr>
							<th>Тип памяток</th>
							<th>Кількість зареєстрованих</th>							
						</tr>						
					</thead>
					<tbody>
						
						
						<?php foreach ($types as $type) {?>
						<tr>
                                <td class="monument_name"><a href="#"><?=$type->name?></a></td>
							<td><?=$type->getMonuments()->count();?></td>
						</tr>
                            <?} ?>
                        


					</tbody>
				</table>
