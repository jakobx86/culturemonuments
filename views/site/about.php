<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Цей сайт був створений в якості дипломного проекту студента 
        Балкрева Якова Володимировича групи КБЗ 513 по спеціальності "Компьютерна Інженерія"
    </p>

    <code><?= __FILE__ ?></code>
</div>
