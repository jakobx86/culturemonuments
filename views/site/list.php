<?
use yii\widgets\LinkPager;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

?>
			<?=$this->render('/partials/side_bar',['popular'=>$popular,'recent'=>$recent]);?>
                <div id="content">
                	<div class="filter">
				<?php $form = ActiveForm::begin(); ?>    
				<div class="filter_list">
				    <?= Html::dropDownList('type', $selectedType, $types, array(
        'prompt' => 'Не вибрано'),['class'=>'target']) ?>
				</div>
				<div class="filter_list">
				    <?= Html::dropDownList('district', $selectedDistrict, $districts,array(
        'prompt' => 'Не вибрано'),['class'=>'target']) ?>
				   
				    
				        <?= Html::submitButton('Вибрати' , ['class' =>'filter_botton','name'=>'filter'])?>
				        <?= Html::submitButton('X' , ['class' =>'filter_botton','name'=>'filter_reset'])?>			        
				    
				 </div>

				 <div id="show-view">
                    <ul>
                        <li><?php $class= ($view=='list' ? 'view-method-default selected' : 'view-method-default');echo Html::a('', ['site/list', 'view' => 'list'], ['class' => $class]); ?>
                            <!--<a href="list.html" class="view-method-default selected"></a>--></li>                
                        <li><?$class= ($view=='grid' ? 'view-method-default selected' : 'view-method-default');echo Html::a('', ['site/list', 'view' => 'grid'], ['class' => $class]); ?></li>
                    </ul>
                    </div>
				<?php ActiveForm::end(); ?>
			</div>
                <br  />
                <?php if ($view=='list') 
/*----------------------------------------------------------------------------------------------------------------------------*/
                {?>

					<?php foreach ($monuments as $monument) {?>
						<div class="monument_item">
						<div class="viewed">
					<i class="fa fa-eye" aria-hidden="true"></i> <span><?php echo $monument->viewed?></span>
						</div>										
                    <a href="<?=Url::toRoute(['site/detail','id'=>$monument->id])?>"><h1><?php echo $monument->name?></h1></a>                    
					<a href="<?=Url::toRoute(['site/detail','id'=>$monument->id])?>"><img style="width: 200px;" src="<?= $monument->getPicture(); ?>" alt=''></a>
					<div class="monumnt_desc">
						<p>Дата події: <?php echo $monument->date_event?></p>
						<p>Автори / Дослідники: <?php echo $monument->authors?></p>
						<p>Місцезнаходження: 	<?php echo $monument->location?></p>
					</div>
				</div>
            <?php }?>
					<?php }
/*----------------------------------------------------------------------------------------------------------------------------*/
                    else 
                    {?>
                        <table>
                    <thead>
                        <tr>
                            <th>Назва Памятки</th>
                            <th>Охоронний номер</th>
                            <th>Дата події</th>
                            <th>Автори/Дослідники</th>
                            <th>Місцезнаходження</th>
                        </tr>                       
                    </thead>
                    <tbody>
                        
                            <?php foreach ($monuments as $monument) {?>
                            <tr>
                                <td class="monument_name"><a href="<?=Url::toRoute(['site/detail','id'=>$monument->id])?>"><?php echo $monument->name?></a></td>
                                <td><?php echo $monument->protect_num?></td>
                                <td><?php echo $monument->date_event?></td>
                                <td><?php echo $monument->authors?></td>
                                <td class="location"><a href="#"><?php echo $monument->location?></a></td>
                                </tr>
                            <?} ?>
                        


                    </tbody>
                </table>
                    <?php 
                    }
/*----------------------------------------------------------------------------------------------------------------------------*/
                    ?>
							
				<?php echo LinkPager::widget([
				    'pagination' => $pagination,
				]);?>
				
			</div>
			
					