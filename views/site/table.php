<h1>Вибрати памятки культури</h1>
					<div class="filter">
					<form>
					<div class="filter_list">						
						<select name="type" class="target" id="type">
					  		<option value="" selected>Тип памятки</option> 
					  		<option value="IPC">Історії</option>
					  		<option value="NVR">Архітектури та Містобудування</option>
					  		<option value="DVR">Археології</option>
					  		<option value="Системный блок">Науки і техніки</option>
					  		<option value="Другое устройство" >Монументального мистецтва</option>
						</select>
					</div>
					<div class="filter_list">						
						<select name="location" class="target" id="location">
					  		<option value="" selected>Район</option> 
					  		<option value="IPC">Історії</option>
					  		<option value="NVR">Архітектури та Містобудування</option>
					  		<option value="DVR">Археології</option>
					  		<option value="Системный блок">Науки і техніки</option>
					  		<option value="Системный блок">Науки і техніки</option>
					  		<option value="Системный блок">Науки і техніки</option>
					  		<option value="Системный блок">Науки і техніки</option>
					  		<option value="Системный блок">Науки і техніки</option>
					  		<option value="Системный блок">Науки і техніки</option>
					  		<option value="Системный блок">Науки і техніки</option>
					  		<option value="Системный блок">Науки і техніки</option>
					  		<option value="Системный блок">Науки і техніки</option>
					  		<option value="Системный блок">Науки і техніки</option>
					  		<option value="Системный блок">Науки і техніки</option>
					  		<option value="Системный блок">Науки і техніки</option>
					  		<option value="Системный блок">Науки і техніки</option>
					  		<option value="Системный блок">Науки і техніки</option>
					  		<option value="Системный блок">Науки і техніки</option>
					  		<option value="Системный блок">Науки і техніки</option>
					  		<option value="Системный блок">Науки і техніки</option>
					  		<option value="Другое устройство" >Монументального мистецтва</option>
						</select>
						<input class="filter_botton" type="submit" name="" value="Вибрати">
					</div>
					<div id="show-view">
		            <ul>
		                <li><a href="list.html" class="view-method-default selected"></a></li>                
		                <li><a href="table.html" class="view-method-table "></a></li>
		            </ul>
        			</div>
				</form>
				</div>
					<br  />
				<table>
					<thead>
						<tr>
							<th>Назва Памятки</th>
							<th>Охоронний номер</th>
							<th>Дата події</th>
							<th>Автори/Дослідники</th>
							<th>Місцезнаходження</th>
						</tr>						
					</thead>
					<tbody>
						<tr>
							<td class="monument_name"><a href="detail.html">Ротонда</a></td>
							<td>155-Р</td>
							<td>Початок ХХ ст.</td>
							<td>Автори/Дослідники</td>
							<td class="location"><a href="#">вул. Набережна (ріг вул. Фалєєвської)</a></td>
						</tr>
						<tr>
							<td class="monument_name"><a href="detail.html">Ротонда</a></td>
							<td>155-Р</td>
							<td>Початок ХХ ст.</td>
							<td>Автори/Дослідники</td>
							<td class="location"><a href="#">вул. Набережна (ріг вул. Фалєєвської)</a></td>
						</tr>
						<tr>
							<td class="monument_name"><a href="detail.html">Ротонда</a></td>
							<td>155-Р</td>
							<td>Початок ХХ ст.</td>
							<td>Автори/Дослідники</td>
							<td class="location"><a href="#">вул. Набережна (ріг вул. Фалєєвської)</a></td>
						</tr>	
						<tr>
							<td class="monument_name"><a href="detail.html">Ротонда</a></td>
							<td>155-Р</td>
							<td>Початок ХХ ст.</td>
							<td>Автори/Дослідники</td>
							<td class="location"><a href="#">вул. Набережна (ріг вул. Фалєєвської)</a></td>
						</tr>
						<tr>
							<td class="monument_name"><a href="detail.html">Ротонда</a></td>
							<td>155-Р</td>
							<td>Початок ХХ ст.</td>
							<td>Автори/Дослідники</td>
							<td class="location"><a href="#">вул. Набережна (ріг вул. Фалєєвської)</a></td>
						</tr>


					</tbody>
				</table>	
				<div class="pagination">
				  <a href="#">&laquo;</a>
				  <a href="#">1</a>
				  <a class="active" href="#">2</a>
				  <a href="#">3</a>
				  <a href="#">4</a>
				  <a href="#">5</a>
				  <a href="#">6</a>
				  <a href="#">&raquo;</a>
				</div>