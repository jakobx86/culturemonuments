<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PostSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="filter">
    <?php $form = ActiveForm::begin([
        'action' => ['grid'],
        'method' => 'post',
        'options'=> '',
    ]); ?>   

    
    <?= $form->field($model, 'type_id')->label(false)->dropDownList( $types, array(
        'prompt' => 'Не вибрано','class'=>'target')) ?>
    
    
    <?= $form->field($model, 'district_id')->label(false)->dropDownList( $districts, array(
        'prompt' => 'Не вибрано')) ?>

        <?= Html::submitButton('Вибрати' , ['class' =>'filter_botton','name'=>'filter'])?>
		<?= Html::resetButton('X' , ['class' =>'filter_botton','name'=>'filter_reset'])?>
    <div id="show-view">
                    <ul>
                        <li><?= Html::a('', ['site/list', 'page' => 'list'], ['class' => 'view-method-default']) ?>
                            <!--<a href="list.html" class="view-method-default selected"></a>--></li>                
                        <li><?= Html::a('', ['site/grid', 'page' => 'grid'], ['class' => 'view-method-default selected']) ?></li>
                    </ul>
                    </div>

    

    <?php ActiveForm::end(); ?>
</div>