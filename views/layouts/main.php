<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\PublicAsset;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\widgets\Menu;
use app\assets\AppAsset;
use yii\widgets\ActiveForm;
use app\models\Admins;
use app\components\ModalLoginForm;

PublicAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div id="bg1"></div>
        <div id="bg2"></div>
        <div id="outer">
            <div id="header">
                <div id="logo">
                    <h1>
                        <a href="index.php">Облік Пам'яток культури Миколаївщини    </a>
                    </h1>
                </div>
                <div id="search">
                    <form name="search" method="post" action="search.php">
                        <div>
                            <input type="search" class="text" name="query" size="32" maxlength="128" />
                        </div>
                    </form>
                </div>
                <nav class="menu_top">
                    <ul>
                        <li >
                            <?= Html::a('Головна', ['site/index']) ?>                            
                        </li>
                            
                        <li>
                            <?= Html::a('Каталог Пам\'яток', ['site/list']) ?> 
                            
                        </li>
                        <li>
                            <?= Html::a('Статистика', ['site/statistic']) ?> 
                            
                        </li>
                        <li>
                            <?= Html::a('Законодавство', ['site/law']) ?>
                        </li>                       
                        <li>
                            <?= Html::a('Про сайт', ['site/about']) ?>
                        </li>
                        <?if (!(Yii::$app->user->isGuest)) { echo ('<li>'. Html::a('Додати Пам\'ятку', ['admin/']).'</li>'); } ?>
                        <li class="login">
                            <i class="fa fa-user" aria-hidden="true"></i><?if(Yii::$app->user->isGuest){?><a href="auth/login">Авторизуватися</a><?php } 
                            else 
                            {?>
                               <a href="auth/logout">Вийти</a>
                            <?}

                            ?>
                        </li>
                    </ul><br class="clear" />
                </nav>
               
            </div>
            <div id="banner">
                <div class="captions">
                    <h2>Культурна спадщина Миколаєва</h2>
                </div>
                <div class="slider">
                <ul>
                    <li><img src="/public/images/admin.jpg" alt=""></li>
                    <li><img src="/public/images/admin.jpg" alt=""></li>
                    <li><img src="/public/images/admin.jpg" alt=""></li>
                    <li><img src="/public/images/admin.jpg" alt=""></li>
                    <li><img src="/public/images/admin.jpg" alt=""></li>
                </ul>
            </div>
                
            </div>
            <div id="main">

                
<?= $content ?>
            
            </div>  
</div>
        
        <footer id="main_footer">
            <div class="footer_inner">@copy right MTU</div>
        </footer>

           

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
