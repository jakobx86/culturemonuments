<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Monuments;
use app\models\MonumentsSearch;
use app\models\Types;
use app\models\Districts;
use yii\helpers\ArrayHelper;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider; 

class SiteController extends Controller
{    
    /**
     * @inheritdoc
     */
    var $session;
    public function init() 
    {
        $this->session = Yii::$app->session;
    }
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $types=Types::find()->all();
        
        return $this->render('index',['types'=>$types]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }  
        
         $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    public function actionLaw()
    {
        return $this->render('law');
    }
     public function actionStatistic()
    {

        $districts=Districts::find()->all();
        return $this->render('statistic',['districts'=>'districts']);
    }
    /*public function actionNew()

    {
         $types=ArrayHelper::map(Types::find()->all(),'id','name');
        $districts=ArrayHelper::map(Districts::find()->all(),'id','name');
      
        $searchModel = new MonumentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('new', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,'types'=>$types,'districts'=>$districts
        ]);
    }*/
     public function actionList()

    {
       


        // build a DB query to get all articles with status = 1
        

         //$selectedType=$monument->type->id;
            $view='list';
         $types=ArrayHelper::map(Types::find()->all(),'id','name');
         $districts=ArrayHelper::map(Districts::find()->all(),'id','name');
         $searchModel = new MonumentsSearch();
        
         if(array_key_exists('view',Yii::$app->request->get()))
            {
                $view=Yii::$app->request->get('view');
            }
        if(Yii::$app->request->isPost)
            {
                //var_dump(Yii::$app->request->post());die;

                if(array_key_exists('filter_reset',Yii::$app->request->post()))
                    {
                        $this->removeSessionVar();
                        $query = Monuments::find();
                    }
                else if(array_key_exists('filter',Yii::$app->request->post()))
                    {                       
                        if(Yii::$app->request->post('type')!='')                
                            $type=Yii::$app->request->post('type');
                            $selectedType=$type;

                            if(Yii::$app->request->post('district')!='') 
                            $district=Yii::$app->request->post('district');
                            $selectedDistrict= $district ;

                            $query=Monuments::getFilter($type,$district);

                            $this->setSessionVar($type,$district);
                 }
                



            }
        else if(isset($this->session['type']) ||  isset($this->session['district']))
         {
            $type = $this->session->get('type');
            $district = $this->session->get('district');

            $selectedType=$type;
            $selectedDistrict= $district ;

            $query=Monuments::getFilter($type,$district);


         }


            else $query = Monuments::find();

            

            $popular= Monuments::getPopular(3);

            $recent= Monuments::getRecent(3);

            $data = Monuments::getAll($query,2);

        return $this->render('list',['pagination'=>$data['pagination'], 'monuments'=>$data['monuments'],'popular'=>$popular,'recent'=>$recent,'types'=>$types,'districts'=>$districts,'selectedType'=>$selectedType, 'selectedDistrict'=>$selectedDistrict,'view'=>$view]);

    }

    function setSessionVar($type,$district)
    {
        $this->session->set('type', $type);
        $this->session->set('district', $district);
    }
   
    function removeSessionVar()
    {
        $this->session->remove('type');
        $this->session->remove('district');
    }

   
    
    public function actionDetail($id)
    {
         $popular= Monuments::getPopular(3);
        $recent= Monuments::getRecent(3);
        //var_dump($id);die;
        $monument=Monuments::findOne($id);
        //var_dump($monument);die;
        return $this->render('detail',['monument_detail'=>$monument,'popular'=>$popular,'recent'=>$recent]);
    }
    public function actionTable()
    {
        return $this->render('table');
    }
}
