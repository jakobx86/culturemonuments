<?php 
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Admins;
use app\models\LoginForm;
/**
* 
*/
class AuthController extends Controller
{
    
    /*function __construct(argument)
    {
        # code...
    }*/

public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }  
        
         $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
     public function actionTest()
    {
        $admin=Admins::findOne(1);

        Yii::$app->user->logout($admin);

        if(Yii::$app->user->isGuest)
            {
                echo('Пользователь Гость');
            }
        else
        {
            echo('Пользователь Авторизован');
        }
        /*echo "<pre>"; print_r(Yii::$app->user); echo "</pre>";die;
        var_dump(Yii::$app->components);*/
    }
}

?>