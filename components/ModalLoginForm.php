<?
namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;
use app\models\LoginForm;


class ModalLoginForm extends Widget
{
    public $authForm ;
    public $hash;
    public function init()
    {
        parent::init();
        
        $this->authForm= new LoginForm();
        //$this->render('views.modalForm','model'=>$this->authForm);

    }

    public function run()
    {
        return $this->render('modalForm',['model'=>$this->authForm,'hash'=>$this->hash]);
    }
}?>