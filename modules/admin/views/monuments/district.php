<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Monuments */
/* @var $form yii\widgets\ActiveForm */	
?>

<div class="monuments-form">

    <?php $form = ActiveForm::begin(); ?>    

    <?= Html::dropDownList('district', $selectedDistrict, $districts,['class'=>'control-form']) ?>

    <div class="form-group">
        <?= Html::submitButton('Submit' , ['class' =>'btn btn-success'])   ?>
        <??>
    </div>

    <?php ActiveForm::end(); ?>

</div>
