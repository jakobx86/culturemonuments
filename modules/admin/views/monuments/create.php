<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Monuments */

$this->title = 'Create Monuments';
$this->params['breadcrumbs'][] = ['label' => 'Monuments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monuments-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
