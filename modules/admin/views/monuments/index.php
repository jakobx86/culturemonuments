<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MonumentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Monuments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monuments-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Monuments', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'protect_num',
            'type_id',
            'name',
            
            [
                'format'=>'html',
                'label'=> 'Image',
                'value'=> function($data)
                {
                    return HTML::img($data->getPicture(),['width'=>200]);  
                }

            ],
             /*'location',
             'district_id',
             'location_map',
             'date_event',
             'date_open',
             'authors',
             'material',
             'size',
             'meaning_id',
             'admin_id',*/

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
