<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Monuments */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Пам\'ятки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monuments-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редагувати', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Завантажити зображення', ['set-image', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Вибрати тип', ['set-type', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Вибрати район', ['set-district', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Видалити', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'protect_num',
            'type_id',
            'name',
            'picture',
            'location',
            'district_id',
            'location_map',
            'date_event',
            'date_open',
            'authors',
            'material',
            'size',
            'meaning_id',
            'admin_id',
        ],
    ]) ?>

</div>
