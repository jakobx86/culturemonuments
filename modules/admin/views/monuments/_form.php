<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Monuments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="monuments-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'protect_num')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>    

    <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date_event')->textInput(['maxlength' => true]) ?> 
    
    <?= $form->field($model, 'date_open')->textInput(['maxlength' => true]) ?>   

    <?= $form->field($model, 'authors')->textInput(['maxlength' => true]) ?>
    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
