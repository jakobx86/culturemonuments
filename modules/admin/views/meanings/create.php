<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Meanings */

$this->title = 'Create Meanings';
$this->params['breadcrumbs'][] = ['label' => 'Meanings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meanings-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
