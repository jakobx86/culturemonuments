<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Monuments;
use app\models\Districts;
use app\models\ImageUpload;
use app\models\MonumentsSearch;
use app\models\Types;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper; 


/**
 * MonumentsController implements the CRUD actions for Monuments model.
 */
class MonumentsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Monuments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MonumentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Monuments model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Monuments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Monuments();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Monuments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Monuments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Monuments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Monuments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Monuments::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSetImage($id)
    {
        $model= new ImageUpload;

        if(Yii::$app->request->isPost)
            {
                $monument= $this->findModel($id);                             
                $file= UploadedFile::getInstance($model,'image');
                
                if($monument->saveImage($model->uploadFile($file,$monument->picture)))
                {
                    return $this->redirect(['view', 'id'=>$monument->id]);
                }

            }

        return $this->render('image',['model'=>$model]);
    }

    public function actionSetType($id)
    {          

         $monument= $this->findModel($id);

         $selectedType=$monument->type->id;

         $types=ArrayHelper::map(Types::find()->all(),'id','name');

         if(Yii::$app->request->isPost)
            {
                $type=Yii::$app->request->post('type');
                if($monument->saveType($type))
                {
                    return $this->redirect(['type','id'=>$monument->id]);
                }

            }

         return $this->render('type',['monument'=>$monument,'selectedType'=>$selectedType, 'types'=>$types]);
    }
    public function actionSetDistrict($id)
    {         

         $monument= $this->findModel($id);
        

         $selectedDistrict=$monument->district->id;

         $districts=ArrayHelper::map(Districts::find()->all(),'id','name');

         if(Yii::$app->request->isPost)
            {
                $district=Yii::$app->request->post('district');                
                if($monument->saveDistrict($district))
                {
                    return $this->redirect(['view','id'=>$monument->id]);
                }

            }

         return $this->render('district',['monument'=>$monument,'selectedDistrict'=>$selectedDistrict, 'districts'=>$districts]);
    }
}
