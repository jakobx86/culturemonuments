<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class PublicAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [       
		'http://fonts.googleapis.com/css?family=Arvo',
		'public/css/font-awesome.min.css',
		'public/style/style.css',
    ];
    public $js = [
    	'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js',
        'public/js/jquery-3.2.1.min.js',
	    'public/js/script.js',
    ];
    public $depends = [
        
    ];
}
